package co.vulpin.stats;

import org.discordbots.api.client.DiscordBotListAPI;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static java.lang.System.getenv;

public class Lambda {

    private static final int TOP_N = 10;

    private InfluxDB influx;
    private DiscordBotListAPI dbl;

    public Lambda() {
        initInflux();
        initDbl();
    }

    private void initInflux() {
        String url = getenv("INFLUXDB_URL");
        String username = getenv("INFLUXDB_USERNAME");
        String password = getenv("INFLUXDB_PASSWORD");
        String database = getenv("INFLUXDB_DATABASE");

        this.influx = InfluxDBFactory.connect(url, username, password)
            .setDatabase(database)
            .enableGzip()
            .enableBatch();

        influx.query(new Query("CREATE DATABASE " + database, database));
    }

    private void initDbl() {
        String token = getenv("DBL_TOKEN");

        this.dbl = new DiscordBotListAPI.Builder()
            .botId("nothing")
            .token(token)
            .build();
    }

    public void handle() {
        CompletableFuture
            .allOf(recordServers(), recordVotes())
            .join();

        influx.flush();
    }

    private CompletableFuture recordServers() {
        List<String> fields = Arrays.asList("server_count", "clientid", "username", "discriminator");

        return dbl.getBots(Collections.emptyMap(), TOP_N, 0, "server_count", fields)
            .thenAccept(result -> {
                result.getResults().stream()
                    .map(b -> {
                        long time = System.currentTimeMillis();
                        return Point.measurement("servers")
                            .addField("amount", b.getServerCount())
                            .tag("username", b.getUsername() + "#" + b.getDiscriminator())
                            .time(time, TimeUnit.MILLISECONDS)
                            .build();
                    })
                    .forEach(influx::write);
            })
            .toCompletableFuture();
    }

    private CompletableFuture recordVotes() {
        List<String> fields = Arrays.asList("monthlyPoints", "clientid", "username", "discriminator");

        return dbl.getBots(Collections.emptyMap(), TOP_N, 0, "monthlyPoints", fields)
            .thenAccept(result -> {
                result.getResults().stream()
                    .map(b -> {
                        long time = System.currentTimeMillis();
                        return Point.measurement("votes")
                            .addField("amount", b.getMonthlyPoints())
                            .tag("username", b.getUsername() + "#" + b.getDiscriminator())
                            .time(time, TimeUnit.MILLISECONDS)
                            .build();
                    })
                    .forEach(influx::write);
            })
            .toCompletableFuture();
    }

}
