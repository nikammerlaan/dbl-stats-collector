import co.vulpin.stats.Lambda;

public class Main {

    public static void main(String[] args) throws Exception {

        Lambda lambda = new Lambda();

        while(true) {
            lambda.handle();
            Thread.sleep(15_000);
        }

    }

}
